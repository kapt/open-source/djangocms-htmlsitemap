from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import include, path

# Local application / specific library imports


admin.autodiscover()

urlpatterns = [path("admin/", admin.site.urls), path("", include("cms.urls"))]

urlpatterns += staticfiles_urlpatterns()
