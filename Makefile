.PHONY: install upgrade coverage travis

install:
		poetry install

upgrade:
		poetry update

coverage:
	poetry run py.test --cov-report term-missing --cov djangocms_htmlsitemap

travis: install coverage
